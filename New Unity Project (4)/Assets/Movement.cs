﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody2D rB2D;

    private float Keys = 0;

    public float runSpeed;
    public float jumpForce;

    float currentZRotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
    
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Key"))
        {
            other.gameObject.SetActive(false);
            Keys = Keys + 1;
        }

        if (other.gameObject.CompareTag("Locked") && Keys != 0)
        {
            Keys = Keys - 1;
            other.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0,0,5));
        }

        if (Input.GetKey(KeyCode.W))
        {
            rB2D.AddRelativeForce(new Vector2(0, 100));
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0,0,-5));
        }
    }}